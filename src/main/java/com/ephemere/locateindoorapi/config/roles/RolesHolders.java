package com.ephemere.locateindoorapi.config.roles;

import com.ephemere.locateindoorapi.model.entities.Utilisateur;

public interface RolesHolders {
    String[] getRoles( Utilisateur utilisateur );
}
