package com.ephemere.locateindoorapi.config.roles;

import com.ephemere.locateindoorapi.model.entities.Utilisateur;

import java.util.Set;

public class AllUsersRolesHolder implements RolesHolders {
    public static final String ROLE_USER = "USER";
    public static final String ROLE_ADMIN = "ADMIN";

    public static Set< String > defaultRoles() {
        return Set.of( ROLE_USER );
    }

    @Override
    public String[] getRoles( Utilisateur utilisateur ) {
        return new String[] { ROLE_USER };
    }
}
