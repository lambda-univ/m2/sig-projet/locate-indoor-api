package com.ephemere.locateindoorapi.config;


import com.ephemere.locateindoorapi.config.filters.JwtAuthenticationFilter;
import com.ephemere.locateindoorapi.config.filters.JwtAuthorizationFilter;
import com.ephemere.locateindoorapi.config.filters.JwtTokens;
import com.ephemere.locateindoorapi.config.roles.AllUsersRolesHolder;
import com.ephemere.locateindoorapi.config.roles.RolesHolders;
import com.ephemere.locateindoorapi.controllers.Controller;
import com.ephemere.locateindoorapi.model.Model;
import com.ephemere.locateindoorapi.model.ModelImplementation;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import javax.crypto.SecretKey;
import java.util.Objects;

@SpringBootConfiguration
public class Config extends WebSecurityConfigurerAdapter {
    public static final String LOGIN_URI = "/login";

    @Autowired
    private JwtTokens jwtTokens;

    @Override
    protected void configure( HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors()
                .and()
                .addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtTokens))
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), jwtTokens))
                .authorizeRequests()
                .antMatchers( "/welcome" ).authenticated()
                .antMatchers( HttpMethod.POST, LOGIN_URI).permitAll()
                .antMatchers( HttpMethod.GET, Controller.UTILISATEUR ).authenticated()
                .antMatchers( HttpMethod.POST, Controller.UTILISATEUR ).permitAll()
                .antMatchers( HttpMethod.GET, Controller.HISTORIQUE ).authenticated()
                .antMatchers( HttpMethod.POST, Controller.ITINERAIRE + "/arrive/**" ).authenticated()

                // allows all requests to be called anonymously: Comment for production
                .antMatchers( "/**" ).permitAll()

                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS );
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        return new UtilisateurUserDetailsService();
    }

    @Bean
    public Model model() {
        return new ModelImplementation();
    }

    @Bean
    public SecretKey getSecretKey() {
        return Keys.secretKeyFor( SignatureAlgorithm.HS256);
    }

    @Bean
    public RolesHolders getRolesHolders() { return new AllUsersRolesHolder(); }




}
