package com.ephemere.locateindoorapi.config;


import com.ephemere.locateindoorapi.config.roles.RolesHolders;
import com.ephemere.locateindoorapi.model.Model;
import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;


public class UtilisateurUserDetailsService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Model model;

    @Autowired
    private RolesHolders rolesHolders;

    @Override
    public UserDetails loadUserByUsername( String pseudo ) throws UsernameNotFoundException {
        try {
            Utilisateur utilisateur = model.getUtilisateur( pseudo );
            User.UserBuilder builder = User.builder();
            builder.username( utilisateur.getPseudo() );
            String requestedPassword = utilisateur.getMotdepasse();
            builder.password( passwordEncoder.encode( requestedPassword ) );
            builder.roles( rolesHolders.getRoles( utilisateur ) );
            UserDetails details = builder.build();
            return details;
        } catch ( NotFoundException e ) {
            throw new UsernameNotFoundException( pseudo );
        }

    }
}
