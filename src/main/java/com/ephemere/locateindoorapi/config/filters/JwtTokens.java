package com.ephemere.locateindoorapi.config.filters;

import com.ephemere.locateindoorapi.config.exceptions.InvalidTokenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtTokens {

    public static final String ROLES_KEY = "roles";
    private static final long EXPIRATION_TIME = 3_600_000;
    private static final String PREFIX = "Bearer ";
    @Autowired
    private Key secretKey;

    public String generate( UserDetails userDetails) {

        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        claims.put(ROLES_KEY, userDetails.getAuthorities().stream().map( GrantedAuthority::getAuthority).collect(Collectors.toList()));

        return PREFIX + Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(secretKey)
                .compact();

    }

    public UsernamePasswordAuthenticationToken decode( String token ) throws InvalidTokenException {
        if (token.startsWith(PREFIX)) {
            token = token.replaceFirst(PREFIX, "");
        }

        try {
            Jws<Claims> jwsClaims = Jwts.parserBuilder().setSigningKey(secretKey).build().parseClaimsJws(token);
            List<String> roles = jwsClaims.getBody().get(ROLES_KEY, List.class);
            List< SimpleGrantedAuthority > authorities = roles.stream().map( SimpleGrantedAuthority::new).collect(Collectors.toList());
            return new UsernamePasswordAuthenticationToken(jwsClaims.getBody().getSubject(), null, authorities);
        } catch ( JwtException e) {
            throw new InvalidTokenException(e.getMessage());
        }

    }
}
