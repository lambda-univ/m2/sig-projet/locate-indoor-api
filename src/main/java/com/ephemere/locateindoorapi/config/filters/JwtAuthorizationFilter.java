package com.ephemere.locateindoorapi.config.filters;

import com.ephemere.locateindoorapi.config.exceptions.InvalidTokenException;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final JwtTokens jwtTokens;

    public JwtAuthorizationFilter( AuthenticationManager authenticationManager, JwtTokens jwtTokens) {

        super(authenticationManager);

        this.jwtTokens = jwtTokens;

    }

    @Override
    protected void doFilterInternal( HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String token = request.getHeader( HttpHeaders.AUTHORIZATION);
        if (token == null) {
            SecurityContextHolder.clearContext();
        } else {
            try {
                SecurityContextHolder.getContext().setAuthentication(jwtTokens.decode(token));
            } catch ( InvalidTokenException e) {
                SecurityContextHolder.clearContext();
            }
        }

        chain.doFilter(request, response);

    }

}
