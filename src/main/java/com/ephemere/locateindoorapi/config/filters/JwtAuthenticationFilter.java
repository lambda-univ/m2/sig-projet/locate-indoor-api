package com.ephemere.locateindoorapi.config.filters;

import com.ephemere.locateindoorapi.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final JwtTokens jwtTokens;
    private final Logger logger = LoggerFactory.getLogger( getClass().getSimpleName() );

    public JwtAuthenticationFilter( AuthenticationManager authenticationManager, JwtTokens jwtTokens) {
        logger.info( "Authentication created" );
        setAuthenticationManager(authenticationManager);

        this.jwtTokens = jwtTokens;

        setFilterProcessesUrl( Config.LOGIN_URI );

    }

    @Override
    protected void successfulAuthentication( HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
        logger.info( "Successful authentication" );
        UserDetails userDetails = ( UserDetails ) authResult.getPrincipal();
        String token = jwtTokens.generate(userDetails);

        response.addHeader( HttpHeaders.AUTHORIZATION, token);
    }

}
