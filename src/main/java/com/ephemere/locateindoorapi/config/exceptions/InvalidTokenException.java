package com.ephemere.locateindoorapi.config.exceptions;

public class InvalidTokenException extends Exception {
    public InvalidTokenException( String message) {
        super(message);
    }
}
