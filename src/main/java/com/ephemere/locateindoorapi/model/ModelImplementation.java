package com.ephemere.locateindoorapi.model;

import com.ephemere.locateindoorapi.DTO.SalleModificationDTO;
import com.ephemere.locateindoorapi.DTO.SalleSendDTO;
import com.ephemere.locateindoorapi.model.entities.*;
import com.ephemere.locateindoorapi.model.exceptions.CreationFailedException;
import com.ephemere.locateindoorapi.model.exceptions.NoHistoryException;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import com.ephemere.locateindoorapi.model.repositories.*;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import jdk.jfr.TransitionTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.transaction.Transactional;
import java.util.*;

@Component
public class ModelImplementation implements Model {

    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private QrcodeRepository qrcodeRepository;
    @Autowired
    private CheminRepository cheminRepository;
    @Autowired
    private SalleRepository salleRepository;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    @Autowired
    private EtageRepository etageRepository;

    @Override
    public Utilisateur getUtilisateur( String pseudo ) throws NotFoundException {
        Utilisateur u = utilisateurRepository.findUtilisateurByPseudo( pseudo );
        if(u == null)
            throw new NotFoundException();
        return u;
    }

    @Override
    public Salle getSalleById(int salleId) throws NotFoundException {
        return salleRepository.findSalleById( salleId );
    }

    @Override
    public Utilisateur createUtilisateur(String nom, String prenom, String pseudo, String motdepasse) throws CreationFailedException
    {
        try
        {
            utilisateurRepository.createUtilisateur(nom,prenom,pseudo,motdepasse);
            return getUtilisateur(pseudo);
        }
        catch (NotFoundException e)
        {
            throw new CreationFailedException();
        }
    }

    public Collection<Qrcode> getAllQrcode()
    {
        return qrcodeRepository.getAllQrcode();
    }

    @Override
    public Collection<Chemin> getAllChemins() {
        return cheminRepository.findAllChemins();
    }

    @Override
    public Collection<Chemin> getCheminByEdge(Point p)
    {
        return cheminRepository.findCheminsByEdge(p);
    }

    @Override
    public Collection<Historique> getHistoriqueOfUtilisateur(Utilisateur utilisateur)
    {
        return historiqueRepository.getHistoriqueOfUtilisateur(utilisateur);
    }

    @Override
    public Collection<Historique> getHistoriqueOfSalle(int idSalle) throws NotFoundException {
        return historiqueRepository.getHistoriqueOfSalle(salleRepository.findSalleById(idSalle));
    }

    @Override
    public Collection<Salle> getAllSalle() { return salleRepository.getAllSalle(); }

    @Override
    public Collection<Etage> getAllEtage() { return etageRepository.getAllEtage(); }

    public Qrcode getQrcodeById(final  int id) throws NotFoundException { return  qrcodeRepository.findQrcodeById(id); }


    @Override
    public Qrcode getClosestQRCodeFromSalle( Salle salle )
    {
        return qrcodeRepository.getQrcodeWithRespectToSalle(salle).get(0);
    }

    @Override
    public Chemin getClosestCheminFromCQRCode( Qrcode qrcode ) throws NotFoundException {
        Optional<Chemin> cheminOptional = cheminRepository.getCheminByClosestWithQrcode(qrcode).stream().findFirst();
        if(cheminOptional.isPresent())
        {
            return cheminOptional.get();
        }
        else
        {
            throw new NotFoundException();
        }
    }

    @Override
    public Point getClosestCheminExtremumFromQrCode( Chemin chemin, Qrcode qrcode )
    {
        GeometryFactory gf = new GeometryFactory();
        Point x = gf.createPoint(chemin.getGeom().getCoordinates()[0]);
        Point y = gf.createPoint(chemin.getGeom().getCoordinates()[1]);

        return qrcode.getPoint().distance(x) >= qrcode.getPoint().distance(y) ? y : x ;
    }

    @Override
    public Collection< Chemin > getAllPathsInEtage( Collection< Etage > etages )
    {
        return cheminRepository.findAllCheminForEtages(etages);
    }

    @Override
    public Collection< Point > getNeighbors( Point point, Etage e1, Etage e2 ) {
        return cheminRepository.getNeighbors( point, e1, e2 );
    }


    @Override
    public Collection< Etage > matchEtagesByNumber( Collection< Integer > etagesNumber )
    {
        Collection<Etage> etages = new ArrayList<>();
        for (Integer integer : etagesNumber)
        {
            etages.add(etageRepository.getEtageByNumbre(integer));
        }

        return etages;
    }

    public Collection< Point > getAllPointsBetweenTwoEtages(Etage e1, Etage e2) {
        return cheminRepository.findAllPointsWithinTwoEtages( e1, e2 );
    }

    @Override
    public double getEuclideanDistanceBetweenPoints(Point p1, Point p2 )
    {
        return Math.sqrt( Math.pow( p1.getX() - p2.getX(), 2 ) + Math.pow( p1.getY() - p2.getY(), 2 ) );
    }


    @Override
    public Chemin findCheminByTwoPoints( Point p1, Point p2 )
    {
        return cheminRepository.findCheminByExtremis(p1,p2);
    }

    @Override
    public Collection< Chemin > dijkstra( Qrcode startQRCode, Salle endSalle ) throws NotFoundException {
        // search given interesting stages
        final Etage startStage = startQRCode.getEtage();
        final Etage endStage = endSalle.getEtage();

        // to execute dijkstra, we need to get start and end points on paths
        Qrcode closestQRCodeFromSalle = getClosestQRCodeFromSalle( endSalle );
        Chemin closestCheminFromSalle = getClosestCheminFromCQRCode( closestQRCodeFromSalle );
        final Point endPoint = getClosestCheminExtremumFromQrCode( closestCheminFromSalle, closestQRCodeFromSalle );

        Chemin closestCheminFromStart = getClosestCheminFromCQRCode( startQRCode );
        final Point startPoint = getClosestCheminExtremumFromQrCode( closestCheminFromStart, startQRCode );

        // Preparing Dijsktra algorithm
        PriorityQueue<DijkstraNode> distanceFromStart = new PriorityQueue<>();
        Map<Point, Point> parentPoint = new HashMap<>();
        parentPoint.put( startPoint, null );
        distanceFromStart.add( new DijkstraNode( startPoint, 0f ) );



        // Applying Dijkstra Algorithm
        while ( !distanceFromStart.isEmpty() ) {
            // found and remove closest point from start
            DijkstraNode closestDijkstraPointFromStart = distanceFromStart.poll();
            final Point point = closestDijkstraPointFromStart.getPoint();
            final double distanceBetweenStartAndPoint = closestDijkstraPointFromStart.getDistance();

            // stop search if current point is last point
            if ( endPoint.equals( point ) ) break;

            for ( Point neighbor : getNeighbors( point, startStage, endStage ) ) {
                // we exclude point we already have match
                if ( parentPoint.containsKey( neighbor ) ) continue;

                // computes distance from start and neighbor, by using current point as reference
                final double distanceBetweenPointAndNeighbor = getEuclideanDistanceBetweenPoints( point, neighbor );
                final double distanceBetweenStartAndNeighbor = distanceBetweenStartAndPoint +
                        distanceBetweenPointAndNeighbor;
                distanceFromStart.add( new DijkstraNode( neighbor, distanceBetweenStartAndNeighbor ) );

                // define current point as neighbor's parent
                parentPoint.put( neighbor, point );
            }
        }

        // build path from end to start
        LinkedList<Chemin> path = new LinkedList<>();
        Point currentPoint = endPoint;
        while ( currentPoint != null ) {
            Point currentPointParent = parentPoint.get( currentPoint );
            if ( currentPointParent != null ) {
                path.addFirst( findCheminByTwoPoints( currentPoint, currentPointParent ) );
            }
            currentPoint = currentPointParent;
        }

        return path;
    }

    @Override
    public void arrived(String pseudo, int idSalle) throws NotFoundException {
        historiqueRepository.addHistorique(
                getUtilisateur(pseudo),
                salleRepository.findSalleById(idSalle));
    }

    @Override
    public Etage findEtageById(int idEtage) throws NotFoundException {
        return etageRepository.findEtageById( idEtage );
    }

    @Override
    public Collection<Utilisateur> getAllUtilisateurs() {
        return utilisateurRepository.findAll();
    }

    @Override
    public Historique findLastHistoriqueByUtilisateur(Utilisateur utilisateur) throws NoHistoryException {
         return historiqueRepository.getLastHistoriqueForUtilisateur( utilisateur.getPseudo() );
    }

    @Override
    @Transactional
    public void updateSalleName(int salleId, SalleModificationDTO salleModificationDTO) throws NotFoundException {
        Salle salle = getSalleById(salleId);
        if ( salleModificationDTO.getNom() != null ) {
            salle.setNom(  salleModificationDTO.getNom() );
        }
    }

}
