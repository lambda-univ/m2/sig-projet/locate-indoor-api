package com.ephemere.locateindoorapi.model.entities;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "salle")
public class Salle
{
    @Id
    @GeneratedValue
    @Column(name = "id")
    private  int id;

    @Column(name = "geom")
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    private Geometry geom;

    @Column(name = "nom")
    private String nom;

    @Column(name = "numero")
    private int numero;

    @ManyToOne
    @JoinColumn(name = "etage_id")
    private Etage etage;

    @ManyToOne()
    @JoinColumn(name = "categorie_id")
    private Categorie categorie;
}
