package com.ephemere.locateindoorapi.model.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.Parameter;


@Getter
@Setter
@Entity
@Table(name = "utilisateur")
public class Utilisateur 
{

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
            name = "sequence-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "user_sequence"),
                    @Parameter(name = "initial_value", value = "4"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "id")
    private int id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "pseudo")
    private String pseudo;

    @Column(name = "motdepasse")
    private String motdepasse;

    @OneToMany(mappedBy = "utilisateur")
    private Set<Historique> historiques;

    public Utilisateur() { historiques = new HashSet<>(); }
}
