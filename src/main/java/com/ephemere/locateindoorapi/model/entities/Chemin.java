package com.ephemere.locateindoorapi.model.entities;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "chemin")
public class Chemin
{
     @Id
     @GeneratedValue
     @Column(name = "id")
     private int id;

     @Column(name = "geom")
     @JsonSerialize(using = GeometrySerializer.class)
     @JsonDeserialize(contentUsing = GeometryDeserializer.class)
     private Geometry geom;

     @ManyToOne
     @JoinColumn(name = "etage_x_id")
     private Etage etage_x;

     @ManyToOne
     @JoinColumn(name = "etage_y_id")
     private Etage etage_y;
}
