package com.ephemere.locateindoorapi.model;

import com.ephemere.locateindoorapi.DTO.SalleModificationDTO;
import com.ephemere.locateindoorapi.DTO.UtilisateurSendDTO;
import com.ephemere.locateindoorapi.model.entities.*;
import com.ephemere.locateindoorapi.model.exceptions.CreationFailedException;
import com.ephemere.locateindoorapi.model.exceptions.NoHistoryException;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import com.vividsolutions.jts.geom.Point;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

public interface Model
{

    /**
     * Get an user by his pseudo
     *
     * @param pseudo of the user that we want to find
     *
     * @return the user with the pseudo given in argument of this method
     *
     * @throws NotFoundException there is no user with this pseudo
     */
    Utilisateur getUtilisateur( String pseudo ) throws NotFoundException;


    /**
     * Create a new user
     *
     * @param nom the lastname of the new user
     * @param prenom the firstname of the new user
     * @param pseudo the pseudo of the new user
     * @param motdepasse the password of the new user
     *
     * @return the user created
     *
     * @throws CreationFailedException the user created isn't in the database
     */
    Utilisateur createUtilisateur( String nom,String prenom, String pseudo, String motdepasse ) throws CreationFailedException;


    /**
     * Return all qrcodes
     *
     * @return a collection of qrcode
     */
    Collection<Qrcode> getAllQrcode();


    /**
     * Return a qrcode by an id given
     *
     * @param id the id of the qrcode that we want to return
     *
     * @return the qrcode with the same id as the id given
     *
     * @throws NotFoundException there is no qrcode with this id
     */
    Qrcode getQrcodeById(final  int id) throws NotFoundException;


    /**
     * Return a room by his id
     *
     * @param salleId the id of the room that we want to return
     *
     * @return the room with the same id as the one given
     *
     * @throws NotFoundException there is no room with this id
     */
    Salle getSalleById( final int salleId ) throws NotFoundException;


    /**
     * Return all paths in the building
     *
     * @return a collection of path
     */
    Collection<Chemin> getAllChemins();


    /**
     * Return a all paths having an end equal to the given point
     *
     * @param p an end of one or more paths
     *
     * @return a collection of path
     */
    Collection<Chemin> getCheminByEdge(Point p);


    /**
     * Return the historic of a user
     *
     * @param utilisateur the user that we want his historic
     *
     * @return a collection of historic
     */
    Collection<Historique> getHistoriqueOfUtilisateur(Utilisateur utilisateur);

    /**
     * Return the historic of a room
     *
     * @param idSalle the id of room that we want it historic
     *
     * @return a collection of historic
     */
    Collection<Historique> getHistoriqueOfSalle(int idSalle) throws NotFoundException;


    /**
     * Return all rooms in the building
     *
     * @return a collection of room
     */
    Collection<Salle> getAllSalle();


    /**
     * Return all floors in th building
     *
     * @return a collection of floor
     */
    Collection<Etage> getAllEtage();


    /**
     * Get closest QRCode From a salle.
     *
     * As a QRCode is a point and salle a polygon, only QRCode out of salle or in border can be returned.
     *
     * @param salle Salle
     *
     * @return Closest QRCode from salle.
     */
    Qrcode getClosestQRCodeFromSalle( Salle salle );


    /**
     * Returns closest Chemin From QRCode.
     *
     * Notice the the returned Chemin is on the same Etage that QRCode.
     *
     * @param qrcode QRCode used as reference to locate closest Chemin.
     *
     * @return Closest Chemin.
     */
    Chemin getClosestCheminFromCQRCode( Qrcode qrcode ) throws NotFoundException;


    /**
     * Returns closest Chemin's extremum from QRCode.
     *
     * As a Chemin as a segment, it has two significant point, one for segment's start and one for segment's end.
     * This method return one of these point, which is the most closest from QRCode.
     *
     * @param chemin Chemin where we need to define which extremum is closest from QRCode
     * @param qrcode Reference point.
     *
     * @return Closest Chemin's extremum from QRCode.
     */
    Point getClosestCheminExtremumFromQrCode( Chemin chemin, Qrcode qrcode );


    /**
     * Returns all Chemins located in one of given Etages.
     *
     * All returned Chemins are <b>distinct</b>.
     *
     * If a Chemin represents a stair between two given Etages, it is returned.
     *
     * @param etages Etages where method search Chemins.
     *
     * @return All Chemins located in one of given Etage.
     */
    Collection<Chemin> getAllPathsInEtage( Collection<Etage> etages );


    /**
     * Returns point's neighbors from a given point, only located in given etages.
     *
     * @param point Point reference.
     * @param e1 floor where method search neighbors.
     * @param e2 floor where method search neighbors
     * @return Point's neighbors.
     */
    Collection<Point> getNeighbors( Point point, Etage e1, Etage e2 );


    /**
     * Returns all etages from etages number.
     *
     * @return All etages from etages number.
     */
    Collection<Etage> matchEtagesByNumber( Collection<Integer> etagesNumber );


    /**
     * Get all point between two floors
     *
     * @param e1 the first floor
     * @param e2 the second floor
     *
     * @return a collection of point
     */
    Collection<Point> getAllPointsBetweenTwoEtages(Etage e1, Etage e2);


    /**
     * Returns euclidean distance between two points.
     *
     * Let's suppose two points P_1 = (x_1, y_1) and P_2 = (x_2, y_2).
     * Euclidean distance is equals to:
     * d = \sqrt{ ( x_1 - x_2 )^2 + ( y_1 - y_2 )^2 }
     *
     *
     * @param p1 First point
     * @param p2 Second point
     *
     * @return Euclidean distance between p1 and p2.
     */
    double getEuclideanDistanceBetweenPoints(Point p1, Point p2);


    /**
     * Returns a chemin where start is p1 or p2 and end is p1 or p2 and p1 not equals to p2.
     *
     * @param p1 First point
     * @param p2 Second point
     *
     * @return the path which has this extremums equals to this points
     */
    Chemin findCheminByTwoPoints( Point p1, Point p2 );

    /**
     * Implement Dijkstra algorithm. Return the best path between two vertex in a graph
     *
     * @param start the current position of the user
     * @param salle the room that we want to go
     *
     * @return a collection of path which create the best path to go to the room given
     */
    Collection<Chemin> dijkstra( Qrcode start, Salle salle ) throws NotFoundException;


    /**
     * Notify that the user is arrived
     *
     * @param pseudo the current user pseudo
     * @param idSalle the room that the user wanted to go
     *
     * @throws NotFoundException there is no room with this id
     */
    void arrived(String pseudo, int idSalle) throws NotFoundException;

    Etage findEtageById(int idEtage) throws NotFoundException;

    Collection<Utilisateur> getAllUtilisateurs();

    Historique findLastHistoriqueByUtilisateur(Utilisateur utilisateur) throws NoHistoryException;

    void updateSalleName(int salleId, SalleModificationDTO salleModificationDTO) throws NotFoundException;
}
