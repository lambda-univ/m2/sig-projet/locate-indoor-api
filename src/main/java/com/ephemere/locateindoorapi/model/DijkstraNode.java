package com.ephemere.locateindoorapi.model;

import com.vividsolutions.jts.geom.Point;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DijkstraNode implements Comparable<DijkstraNode> {
    private Point point;
    private double distance;

    @Override
    public int compareTo( DijkstraNode o ) {
        return Double.compare( distance, o.distance );
    }
}
