package com.ephemere.locateindoorapi.model.repositories;

import com.ephemere.locateindoorapi.model.entities.Qrcode;
import com.ephemere.locateindoorapi.model.entities.Salle;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Component
public class SalleRepository
{

    @PersistenceContext
    private EntityManager entityManager;

    public Collection<Salle> getAllSalle()
    {
        return entityManager.createQuery("select s from Salle s",Salle.class).getResultList();
    }

    public Salle findSalleById(int id) throws NotFoundException {
        Salle salle = entityManager.createQuery( "SELECT s FROM Salle s WHERE s.id = :id", Salle.class )
                .setParameter( "id", id )
                .getSingleResult();
        if ( salle == null ) throw new NotFoundException("Salle not found with id " + id);
        return salle;
    }

}
