package com.ephemere.locateindoorapi.model.repositories;

import com.ephemere.locateindoorapi.model.entities.Chemin;
import com.ephemere.locateindoorapi.model.entities.Etage;
import com.ephemere.locateindoorapi.model.entities.Qrcode;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CheminRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public Chemin findCheminById(final int id) {
        return entityManager.find(Chemin.class, id);
    }


    public Collection<Chemin> findAllChemins() {
        return entityManager.createQuery("select c from Chemin c", Chemin.class).getResultList();
    }

    public Collection<Chemin> findCheminsByEdge(Point p) {
        return entityManager
                .createNativeQuery("select * from Chemin c where ST_Intersects(c.geom,:geom)"
                        , Chemin.class)
                .setParameter("geom", p)
                .getResultList();
    }

    public Collection<Chemin> getCheminByClosestWithQrcode(Qrcode q) {
        return entityManager
                .createNativeQuery(
                        "select " +
                                "c.id,c.geom,c.etage_x_id,c.etage_y_id " +
                                "from " +
                                "( select id,geom,etage_id " +
                                "from " +
                                "Qrcode q " +
                                "where q.id = :id " +
                                "  ) as q1 " +
                                " join " +
                                "Chemin c " +
                                " on (q1.etage_id = c.etage_x_id or q1.etage_id = c.etage_y_id) " +
                                "order by ST_Distance(q1.geom,c.geom)"
                        , Chemin.class
                )
                .setParameter("id", q.getId())
                .getResultList();
    }

    public Collection<Chemin> findAllCheminForEtages(Collection<Etage> etages) {

        Collection<Integer> etageIds = etages.stream().map(Etage::getId).collect(Collectors.toList());

        return entityManager.createQuery(
                "SELECT DISTINCT c from Chemin c where c.etage_x.id in :etage_id or c.etage_y.id in :etage_id"
                , Chemin.class
        )
                .setParameter("etage_id", etageIds)
                .getResultList();
    }

    public Chemin findCheminByExtremis(Point a, Point b) {
        return (Chemin) entityManager.createNativeQuery(
                "select c.id,c.geom,c.etage_x_id,c.etage_y_id  " +
                        "from Chemin c where " +
                        "(( ST_Equals(ST_StartPoint(c.geom),:a) and ST_Equals(ST_EndPoint(c.geom),:b) ) " +
                        "or ( ST_Equals(ST_StartPoint(c.geom),:b) and ST_Equals(ST_EndPoint(c.geom),:a) ))"
                , Chemin.class
        )
                .setParameter("a", a)
                .setParameter("b", b)
                .getSingleResult();
    }

    public Collection<Point> getNeighbors(Point point, Etage e1, Etage e2) {

        Collection<Chemin> chemins = entityManager.createNativeQuery( "Select * FROM Chemin c " +
                "WHERE (c.etage_x_id in :etages AND c.etage_y_id in :etages ) AND ( st_equals(:point, st_startpoint(c.geom)) OR st_equals(:point, st_endpoint(c.geom)))", Chemin.class )
                .setParameter("etages", Arrays.asList(e1.getId(), e2.getId()) )
                .setParameter("point", point)
                .getResultList();

        Set<Point> points = new TreeSet<>();
        for ( Chemin chemin : chemins ) {
            LineString line = (LineString)chemin.getGeom();
            if ( line.getStartPoint().equals( point ) ) {
                points.add(line.getEndPoint());
            } else {
                points.add(line.getStartPoint());
            }
        }

        return new LinkedList<>(points);


        /*
        Collection<Integer> etageIds = Arrays.asList( e1.getId(), e2.getId() );
        return entityManager.createNativeQuery("SELECT DISTINCT st_endpoint(c1.geom) from Chemin c1 WHERE ( c1.etage_x_id in :etage_id or c1.etage_y_id in :etage_id )" +
                "    AND st_equals( st_startpoint(geom, :point)" +
                "UNION SELECT DISTINCT st_startpoint(c2.geom) from Chemin c2 WHERE ( c2.etage_x_id in :etage_id or c2.etage_y_id in :etage_id )" +
                "    AND st_equals( st_endpoint(geom), :point) ", Point.class)
                .setParameter( "point", point )
                .setParameter( "etage_id", etageIds )
                .getResultList();
         */

        //Collection<Point> points = this.findAllPointsWithinTwoEtages(e1, e2);

    }

    public Collection<Point> findAllPointsWithinTwoEtages(Etage e1, Etage e2) {
        /*
        Collection<Integer> etageIds = Arrays.asList( e1.getId(), e2.getId() );
        return entityManager.createNativeQuery("SELECT DISTINCT c1.geom " +
                "FROM Chemin c1 " +
                "WHERE ( c1.etage_x_id in :etage_id or c1.etage_y_id in :etage_id )" +
                "UNION " +
                "SELECT DISTINCT c2.geom" +
                "FROM Chemin c2 " +
                "WHERE ( c2.etage_x_id in :etage_id or c2.etage_y_id in :etage_id )", Point.class)
                .setParameter( "etage_id", etageIds )
                .getResultList();


         */


        Set<Point> points = new TreeSet<>();
        points.addAll( findAllPointsInEtage(e1) );
        points.addAll( findAllPointsInEtage(e2) );
        points.addAll( findAllPointsBetweenEtages(e1, e2) );
        return points;
    }

    public Collection<Point> findAllPointsInEtage( Etage etage ) {
        Collection<Chemin> chemins = entityManager.createQuery(
                "Select c FROM Chemin c WHERE c.etage_x.id = :id AND c.etage_y.id = :id"
                , Chemin.class
                )
                .setParameter("id", etage.getId() )
                .getResultList();

        return getPointsFromPaths(chemins);
    }

    public Collection<Point> findAllPointsBetweenEtages( Etage e1, Etage e2 ) {
        Collection<Chemin> chemins = entityManager.createQuery( "Select c FROM Chemin c " +
                "WHERE (c.etage_x.id = :e1_id AND c.etage_y.id = :e2_id) OR (c.etage_x.id = :e2_id AND c.etage_y.id = :e1_id)"
                , Chemin.class)
                .setParameter("e1_id", e1.getId() )
                .setParameter( "e2_id", e2.getId() )
                .getResultList();

        return getPointsFromPaths(chemins);
    }

    private Collection<Point> getPointsFromPaths(Collection<Chemin> chemins) {
        Set<Point> points = new TreeSet<>();
        for ( Chemin chemin : chemins ) {
            LineString line = (LineString)chemin.getGeom();
            points.add(line.getStartPoint());
            points.add(line.getEndPoint());
        }

        return new LinkedList<>(points);
    }

}
