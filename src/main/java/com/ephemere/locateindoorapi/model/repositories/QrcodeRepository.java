package com.ephemere.locateindoorapi.model.repositories;

import com.ephemere.locateindoorapi.model.entities.Qrcode;
import java.util.*;

import com.ephemere.locateindoorapi.model.entities.Salle;
import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
public class QrcodeRepository
{

    @PersistenceContext
    private EntityManager entityManager;

    public Qrcode findQrcodeById(final int id) throws NotFoundException
    {
        Qrcode qrcode = entityManager.find(Qrcode.class,id);
        if ( qrcode == null ) {
            throw new NotFoundException("QRCode not found");
        }
        return qrcode;
    }

    public Collection<Qrcode> getAllQrcode()
    {
        return entityManager.createQuery("select c from Qrcode c",Qrcode.class).getResultList();
    }

    public Collection<Qrcode> findQrcodeFromFloorNum(final int num)
    {
        return entityManager.createQuery("select c from Qrcode c where c.etage.num = :num",Qrcode.class)
                .setParameter("num",num)
                .getResultList();
    }

    public List<Qrcode> getQrcodeWithRespectToSalle(Salle s)
    {
        return entityManager.createNativeQuery(
                "select q.id,q.geom,q.etage_id from " +
                        "( select id,geom,etage_id from Salle s where s.nom = :salle ) as s1 " +
                        "join " +
                        "Qrcode q " +
                        "on s1.etage_id = q.etage_id " +
                        "where ST_Within(q.geom,s1.geom) = False " +
                        "order by ST_Distance(s1.geom,q.geom)"
                ,Qrcode.class
        )
                .setParameter("salle",s.getNom())
                .getResultList();
    }

}
