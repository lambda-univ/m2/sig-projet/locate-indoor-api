package com.ephemere.locateindoorapi.model.repositories;

import com.ephemere.locateindoorapi.model.entities.Historique;
import com.ephemere.locateindoorapi.model.entities.Qrcode;
import com.ephemere.locateindoorapi.model.entities.Salle;
import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import com.ephemere.locateindoorapi.model.exceptions.NoHistoryException;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import com.sun.source.tree.WhileLoopTree;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;

@Component
public class HistoriqueRepository
{
    @PersistenceContext
    EntityManager entityManager;

    public Collection<Historique> getHistoriqueOfUtilisateur(Utilisateur utilisateur)
    {
        return entityManager
                .createQuery("select h from Historique h where h.utilisateur.pseudo = :pseudo",Historique.class)
                .setParameter("pseudo",utilisateur.getPseudo())
                .getResultList();
    }

    public Collection<Historique> getHistoriqueOfSalle(Salle salle)
    {
        return entityManager
                .createQuery("select h from Historique h where h.salle.id = :id",Historique.class)
                .setParameter("id",salle.getId())
                .getResultList();
    }

    @Transactional
    public void addHistorique(Utilisateur utilisateur, Salle salle) {
        Historique historique = new Historique();
        historique.setDate(new Date());
        historique.setSalle(salle);
        historique.setUtilisateur(utilisateur);

        entityManager.persist(historique);
    }

    public Historique getLastHistoriqueForUtilisateur(String pseudo) throws NoHistoryException {
        try {
            return entityManager.createQuery("SELECT h FROM Utilisateur u JOIN u.historiques h WHERE u.pseudo = :pseudo ORDER BY h.date DESC", Historique.class)
                    .setParameter("pseudo", pseudo)
                    .setMaxResults( 1 )
                    .getSingleResult();
        } catch ( NoResultException e) {
            throw new NoHistoryException();
        }
    }
}
