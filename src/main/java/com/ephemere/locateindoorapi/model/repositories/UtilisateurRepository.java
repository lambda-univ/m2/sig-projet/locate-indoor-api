package com.ephemere.locateindoorapi.model.repositories;

import com.ephemere.locateindoorapi.DTO.UtilisateurSendDTO;
import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Collection;

@Component
public class UtilisateurRepository {

    @PersistenceContext
    private EntityManager liEntityManager;

    public Utilisateur findUtilisateurByPseudo( final String pseudo ) {
        try
        {
            return (Utilisateur) liEntityManager.createQuery( "select u from Utilisateur u where u.pseudo = :pseudo" )
                    .setParameter( "pseudo", pseudo )
                    .getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
    }

    @Transactional
    public void createUtilisateur(String nom, String prenom, String pseudo, String motdepasse)
    {
        if(findUtilisateurByPseudo(pseudo) == null) {
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setNom(nom);
            utilisateur.setPrenom(prenom);
            utilisateur.setPseudo(pseudo);
            utilisateur.setMotdepasse(motdepasse);
            liEntityManager.persist(utilisateur);
        }
    }

    public Collection<Utilisateur> findAll() {
        return liEntityManager.createQuery( "Select u From Utilisateur u" ).getResultList();
    }
}
