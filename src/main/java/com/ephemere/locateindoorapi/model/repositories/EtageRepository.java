package com.ephemere.locateindoorapi.model.repositories;

import com.ephemere.locateindoorapi.model.entities.Etage;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Component
public class EtageRepository
{
    @PersistenceContext
    private EntityManager entityManager;

    public Collection<Etage> getAllEtage()
    {
        return entityManager.createQuery("select e from Etage e",Etage.class).getResultList();
    }

    public Etage getEtageByNumbre(int num)
    {
        return entityManager.createQuery(
                "select e from Etage e where e.num = :num"
                ,Etage.class
        )
                .setParameter("num",num)
                .getSingleResult();
    }

    public Etage findEtageById(int idEtage) throws NotFoundException {
        final Etage etage = entityManager.find(Etage.class, idEtage);
        if ( etage != null ) {
            return etage;
        } else {
            throw new NotFoundException( "Etage not found" );
        }
    }
}
