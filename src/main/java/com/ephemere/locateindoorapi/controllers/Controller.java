package com.ephemere.locateindoorapi.controllers;

import com.ephemere.locateindoorapi.DTO.*;
import com.ephemere.locateindoorapi.model.Model;
import com.ephemere.locateindoorapi.model.entities.*;
import com.ephemere.locateindoorapi.model.exceptions.CreationFailedException;
import com.ephemere.locateindoorapi.model.exceptions.NoHistoryException;
import com.ephemere.locateindoorapi.model.exceptions.NotFoundException;
import com.vividsolutions.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*", allowCredentials = "true")
@RestController
public class Controller {

    public static final String ITINERAIRE = "/itineraire";
    public static final String QRCODE = "/qrcode";
    public static final String UTILISATEUR = "/utilisateur";
    public static final String UTILISATEURS = "/utilisateurs";
    public static final String SALLE = "/salle";
    public static final String HISTORIQUE = "/historique";
    private static final String ETAGE = "/etage";

    private final Logger logger = LoggerFactory.getLogger( getClass().getSimpleName() );

    @Autowired
    Model model;

    /*@GetMapping( value = "/works", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity< String > works() {
        return ResponseEntity.ok( "It's works !" );
    }

    @GetMapping( value = "/welcome", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity< String > welcome( Principal principal ) {
        final String pseudo = principal.getName();
        return ResponseEntity.ok( "Welcome " + pseudo + " !" );
    }


    @GetMapping( value = "/test", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Qrcode> getAllChemins ()
    {
        GeometryFactory gf = new GeometryFactory();
        Point p = gf.createPoint(new Coordinate(5.0,-7.0));
        List<Chemin> cul = (List<Chemin>) model.getCheminByEdge(p);
        Qrcode c = model.getClosestQRCodeFromSalle(model.getAllSalle().stream().findFirst().get());
        Chemin ch = null;
        try {
            Qrcode q = model.getQrcodeById(3);
            ch = model.getClosestCheminFromCQRCode(q);
            Point p1 = model.getClosestCheminExtremumFromQrCode(ch,q);
            Collection<Integer> integers = new ArrayList<>();
            integers.add(1);
            return ResponseEntity.ok(q);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }*/


    @GetMapping( value = UTILISATEURS, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<UtilisateurSendDTO>> getAllUsers( Principal principal ) {
        return ResponseEntity.ok(
                model.getAllUtilisateurs()
                    .stream()
                    .map( UtilisateurSendDTO::create )
                    .collect(Collectors.toList())
        );
    }


    @GetMapping( value = UTILISATEUR, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<UtilisateurSendDTO> getCurrentUtilisateur(Principal principal)
    {
        logger.info("Retreiving user");
        try
        {
            final UtilisateurSendDTO utilisateur = UtilisateurSendDTO.create(model.getUtilisateur(principal.getName()));
            return ResponseEntity.ok(utilisateur);
        }
        catch (NotFoundException e)
        {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping( value = UTILISATEUR, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<UtilisateurSendDTO> createUtilisateur(@RequestBody UtilisateurRetrieveDTO utilisateurRetrieveDTO)
    {
        try {
            Utilisateur utilisateur = model.createUtilisateur(
              utilisateurRetrieveDTO.getNom(),
              utilisateurRetrieveDTO.getPrenom(),
              utilisateurRetrieveDTO.getPseudo(),
              utilisateurRetrieveDTO.getMotdepasse()
            );
            UtilisateurSendDTO createdUtilisateur = UtilisateurSendDTO.create(utilisateur);
            return ResponseEntity.created(URI.create("")).body(createdUtilisateur);
        } catch (CreationFailedException e) {
            return  ResponseEntity.badRequest().build();
        }

    }

    @GetMapping( value = QRCODE + "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<QrcodeSendDTO> getSpecificQrcode(@PathVariable int id)
    {
        try
        {
            final QrcodeSendDTO qrcode = QrcodeSendDTO.create(model.getQrcodeById(id));
            return ResponseEntity.ok(qrcode);
        }
        catch (NotFoundException e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping( value = ITINERAIRE + "/arrive/{idSalle}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity arrived(@PathVariable int idSalle, Principal principal)
    {
        try {
            model.arrived(principal.getName(),idSalle);
            return ResponseEntity.ok().build();
        }
        catch (NotFoundException e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping( value = SALLE, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<SalleSendDTO>> getAllSalle()
    {
        return ResponseEntity.ok(model
                .getAllSalle()
                .stream()
                .map(SalleSendDTO::create)
                .collect(Collectors.toList()));
    }

    @GetMapping( value = "/closestQrcode/{idSalle}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Qrcode> getClosestQrcodeFromSalle(@PathVariable int idSalle)
    {
        try {
            return ResponseEntity.ok(model.getClosestQRCodeFromSalle(model.getSalleById(idSalle)));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping( value = HISTORIQUE, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<HistoriqueSendDTO>> getHistoriqueOfCurrentUtilisateur(Principal principal)
    {
        try
        {
            return ResponseEntity
                    .ok(model
                            .getHistoriqueOfUtilisateur(model.getUtilisateur(principal.getName()))
                            .stream()
                            .map(HistoriqueSendDTO::create)
                            .collect(Collectors.toList()));
        }
        catch (NotFoundException e)
        {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping(value="/historique/salle/{pseudo}",produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<LastHistoriqueDTO> getLastSalle(@PathVariable String pseudo ) {
        try {
            // search utilisateur
            Utilisateur utilisateur = model.getUtilisateur(pseudo);
            Historique historique = model.findLastHistoriqueByUtilisateur( utilisateur );

            LastHistoriqueDTO lastHistoriqueDTO = LastHistoriqueDTO.builder()
                    .date(historique.getDate())
                    .nom(historique.getSalle().getNom())
                    .build();
            return ResponseEntity.ok(lastHistoriqueDTO);
        } catch ( NotFoundException e ) {
            return ResponseEntity.notFound().build();
        } catch ( NoHistoryException e ) {
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping( value=ITINERAIRE, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<CheminSendDTO>> getItineraire(@RequestBody ItineraireRequestDTO itineraireRequestDTO) {
        // checks parameters
        if ( !( itineraireRequestDTO.hasSalleId() && itineraireRequestDTO.hasQRCodeId() ) ) {
            return ResponseEntity.badRequest().build();
        }

        // computes requested path
        try {
            final Salle salle = model.getSalleById(itineraireRequestDTO.getSalleId());
            final Qrcode qrcode = model.getQrcodeById(itineraireRequestDTO.getQrcodeId());
            return ResponseEntity.ok( model.dijkstra( qrcode, salle )
                    .stream()
                    .map(CheminSendDTO::create)
                    .collect(Collectors.toList()));
        } catch (NotFoundException e) {
            System.out.println( e.getMessage() );
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping( value = HISTORIQUE + "/{idSalle}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<HistoriqueSendDTO>> getHistoriqueOfSalle(@PathVariable int idSalle)
    {
        try {
            return ResponseEntity.ok(model.getHistoriqueOfSalle(idSalle)
                    .stream()
                    .map(HistoriqueSendDTO::create)
                    .collect(Collectors.toList()));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping( value = ETAGE, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Etage>> getAllEtages()
    {
        return ResponseEntity.ok(new ArrayList<>(model.getAllEtage()));
    }

    @GetMapping( value = ETAGE + "/{idEtage}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Etage> getEtage( @PathVariable int idEtage ) {
        try {
            final Etage etage = model.findEtageById( idEtage );
            return ResponseEntity.ok(etage);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping(value = "/geom/salle", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Geometry>> getSallesGeomtry() {
        return ResponseEntity.ok(
                model.getAllSalle().stream().map( Salle::getGeom ).collect( Collectors.toList() )
        );
    }


    @GetMapping( value = "/salle/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<SalleSendDTO> getSalle(@PathVariable int id) {
        System.out.println("Getting salle by id: " + id);
        try {
            return  ResponseEntity.ok(SalleSendDTO.create(model.getSalleById(id)));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping( value = "/salle/{salleId}", consumes= {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<SalleSendDTO> updateSalle( @PathVariable int salleId, @RequestBody SalleModificationDTO salleModificationDTO ) {
        try {
            model.updateSalleName( salleId, salleModificationDTO );
            Salle salle = model.getSalleById( salleId );
            return ResponseEntity.ok( SalleSendDTO.create(salle) );
        } catch ( NotFoundException e ) {
            return ResponseEntity.notFound().build();
        }
    }

}
