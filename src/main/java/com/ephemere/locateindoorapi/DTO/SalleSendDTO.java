package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Salle;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Getter;

@Getter
public class SalleSendDTO
{

    private final int id;
    private final int numero;
    private final String nom;
    private final EtageQrcodeSendDTO etage;
    private final CategorieSendDTO categorie;
    private final Geometry geom;

    public SalleSendDTO(Salle salle)
    {
        this.id = salle.getId();
        this.numero = salle.getNumero();
        this.nom = salle.getNom();
        this.categorie = CategorieSendDTO.create(salle.getCategorie());
        this.etage = EtageQrcodeSendDTO.create(salle.getEtage());
        this.geom = salle.getGeom();
    }

    public static  SalleSendDTO create(Salle salle)
    {
        return new SalleSendDTO(salle);
    }
}
