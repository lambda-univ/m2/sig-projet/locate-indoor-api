package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Qrcode;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Getter;

@Getter
public class QrcodeSendDTO
{
    private final int id;
    private final Geometry point;
    private final EtageQrcodeSendDTO etage;

    public QrcodeSendDTO(Qrcode qrcode)
    {
        this.id = qrcode.getId();
        this.etage = EtageQrcodeSendDTO.create(qrcode.getEtage());
        this.point = qrcode.getPoint();
    }

    public static  QrcodeSendDTO create(Qrcode qrcode)
    {
        return new QrcodeSendDTO(qrcode);
    }
}
