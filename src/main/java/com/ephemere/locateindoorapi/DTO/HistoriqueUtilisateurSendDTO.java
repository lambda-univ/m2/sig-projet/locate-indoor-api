package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Historique;
import com.ephemere.locateindoorapi.model.entities.Salle;
import lombok.Getter;
import java.util.Date;

@Getter
public class HistoriqueUtilisateurSendDTO
{
    private final int id;
    private final Date date;
    private final SalleSendDTO salle;

    public HistoriqueUtilisateurSendDTO(Historique historique)
    {
        this.id = historique.getId();
        this.date = historique.getDate();
        this.salle = SalleSendDTO.create(historique.getSalle());
    }

    public static  HistoriqueUtilisateurSendDTO create(Historique historique)
    {
        return new HistoriqueUtilisateurSendDTO(historique);
    }
}
