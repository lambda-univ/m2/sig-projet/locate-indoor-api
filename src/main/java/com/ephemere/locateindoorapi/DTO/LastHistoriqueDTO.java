package com.ephemere.locateindoorapi.DTO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder
public class LastHistoriqueDTO {
    private Date date;
    private String nom;

}
