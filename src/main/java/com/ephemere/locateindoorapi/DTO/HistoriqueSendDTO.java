package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Historique;
import lombok.Getter;
import java.util.Date;

@Getter
public class HistoriqueSendDTO
{
    private final int id;
    private final Date date;
    private final UtilisateurHistoriqueSendDTO utilisateur;
    private final SalleSendDTO salle;

    public HistoriqueSendDTO(Historique historique)
    {
        this.id = historique.getId();
        this.date = historique.getDate();
        this.utilisateur = UtilisateurHistoriqueSendDTO.create(historique.getUtilisateur());
        this.salle = SalleSendDTO.create(historique.getSalle());
    }

    public static  HistoriqueSendDTO create(Historique historique)
    {
        return new HistoriqueSendDTO(historique);
    }
}
