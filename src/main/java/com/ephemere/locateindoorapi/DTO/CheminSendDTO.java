package com.ephemere.locateindoorapi.DTO;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.ephemere.locateindoorapi.model.entities.Chemin;
import com.ephemere.locateindoorapi.model.entities.Etage;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Getter;

@Getter
public class CheminSendDTO
{
    private final int id;
    private final Geometry geom;
    private final EtageQrcodeSendDTO etage_x;
    private final EtageQrcodeSendDTO etage_y;


    public CheminSendDTO(Chemin chemin)
    {
        this.id = chemin.getId();
        this.geom = chemin.getGeom();
        this.etage_x = EtageQrcodeSendDTO.create(chemin.getEtage_x());
        this.etage_y = EtageQrcodeSendDTO.create(chemin.getEtage_y());
    }

    public static  CheminSendDTO create(Chemin chemin)
    {
        return new CheminSendDTO(chemin);
    }
}
