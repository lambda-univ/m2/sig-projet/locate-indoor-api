package com.ephemere.locateindoorapi.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItineraireRequestDTO {
    private static final int UNSPECIFIED = -1;

    private int salleId;
    private int qrcodeId;

    public ItineraireRequestDTO() {
        this.salleId = UNSPECIFIED;
        this.qrcodeId = UNSPECIFIED;
    }

    public boolean hasSalleId() {
        return salleId != UNSPECIFIED;
    }

    public boolean hasQRCodeId() {
        return qrcodeId != UNSPECIFIED;
    }
}
