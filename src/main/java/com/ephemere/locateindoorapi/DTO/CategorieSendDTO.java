package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Categorie;
import lombok.Getter;

@Getter
public class CategorieSendDTO
{

    private final int id;
    private final String nom;
    private final String couleur;

    public CategorieSendDTO(Categorie categorie)
    {
        this.id = categorie.getId();
        this.nom = categorie.getNom();
        this.couleur = categorie.getCouleur();
    }

    public static  CategorieSendDTO create(Categorie categorie)
    {
        return new CategorieSendDTO(categorie);
    }
}
