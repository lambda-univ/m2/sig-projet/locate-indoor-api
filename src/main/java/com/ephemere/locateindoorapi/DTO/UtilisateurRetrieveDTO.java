package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
public class UtilisateurRetrieveDTO
{
    private String nom;
    private String prenom;
    private String pseudo;
    private String motdepasse;

    public UtilisateurRetrieveDTO(Utilisateur utilisateur)
    {
        this.nom = utilisateur.getNom();
        this.prenom = utilisateur.getPrenom();
        this.pseudo = utilisateur.getPseudo();
        this.motdepasse = utilisateur.getPseudo();
    }

    public static  UtilisateurRetrieveDTO create(Utilisateur utilisateur)
    {
        return new UtilisateurRetrieveDTO(utilisateur);
    }
}
