package com.ephemere.locateindoorapi.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalleModificationDTO {
    private String nom;
}
