package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Etage;
import lombok.Getter;

@Getter
public class EtageQrcodeSendDTO
{

    private final int id;
    private final String nom;
    private final int num;

    public EtageQrcodeSendDTO(Etage etage)
    {
        this.id = etage.getId();
        this.nom = etage.getNom();
        this.num = etage.getNum();
    }

    public static  EtageQrcodeSendDTO create(Etage etage)
    {
        return new EtageQrcodeSendDTO(etage);
    }

}
