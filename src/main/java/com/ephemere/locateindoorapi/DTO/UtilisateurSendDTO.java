package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import lombok.Getter;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class UtilisateurSendDTO {

    private final String nom;
    private final String prenom;
    private final String pseudo;
    private final Set<HistoriqueUtilisateurSendDTO> historiques;

    public UtilisateurSendDTO(Utilisateur utilisateur)
    {
        this.nom = utilisateur.getNom();
        this.prenom = utilisateur.getPrenom();
        this.pseudo = utilisateur.getPseudo();
        this.historiques = utilisateur.getHistoriques()
                .stream()
                .map(HistoriqueUtilisateurSendDTO::create)
                .collect(Collectors.toSet());
    }

    public static  UtilisateurSendDTO create(Utilisateur utilisateur)
    {
        return new UtilisateurSendDTO(utilisateur);
    }
}
