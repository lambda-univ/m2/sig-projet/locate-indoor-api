package com.ephemere.locateindoorapi.DTO;

import com.ephemere.locateindoorapi.model.entities.Utilisateur;
import lombok.Getter;

@Getter
public class UtilisateurHistoriqueSendDTO
{
    private final String nom;
    private final String prenom;
    private final String pseudo;

    public UtilisateurHistoriqueSendDTO(Utilisateur utilisateur)
    {
        this.nom = utilisateur.getNom();
        this.prenom = utilisateur.getPrenom();
        this.pseudo = utilisateur.getPseudo();
    }

    public static  UtilisateurHistoriqueSendDTO create(Utilisateur utilisateur)
    {
        return new UtilisateurHistoriqueSendDTO(utilisateur);
    }
}
